import { Router } from 'express';
import controller from '../controllers/currencies';
const router = Router();

router.get('/currencies', controller.get);

export default router;