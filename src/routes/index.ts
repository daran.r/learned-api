import { Express, Request, Response } from 'express';
import currenciesRoutes from './currencies';

export default (app: Express) => {
    app.get('/', (req: Request, res: Response) => {
        res.send('Hello !!!')
    });
    
    app.use('/', currenciesRoutes);
}