import express, { Express, NextFunction, Request, Response } from 'express';
import { assignRequestId, useLogger } from './modules/logger'
import routes from './routes';

const app: Express = express();
const port = process.env.PORT || 8000;

app.use(assignRequestId);
app.use(useLogger)

routes(app);

app.use((req: Request, res: Response) => {
  res.sendStatus(404);
});

app.use((err: Error, req: Request, res: Response, next: NextFunction) => {
  console.error(err.stack)
  res.status(500).send({error: 'something went wrong'})
})

app.listen(port, () => {
  console.log(`Server is listening on https://localhost:${port}`,);
});