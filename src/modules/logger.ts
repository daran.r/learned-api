import { Request, Response, NextFunction } from 'express';
import morgan from 'morgan';
import { v4 as uuidv4 } from 'uuid';

morgan.token('id', (req: Request) => req.id);

const assignRequestId = (req: Request, res: Response, next: NextFunction) => {
    req.id = uuidv4();
    next();
}

const useLogger = morgan(":id :method :url :status :res[content-length] - :response-time ms");

export {
    assignRequestId,
    useLogger
};