export interface CurrencySoapResponseEnvelope {
    body: CurrencySoapResponseBody
}

interface CurrencySoapResponseBody {
    listOfCurrenciesByNameResponse: CurrencySoapResponse
}

interface CurrencySoapResponse {
    listOfCurrenciesByNameResult: CurrencySoapResult
}

interface CurrencySoapResult {
    tCurrency: Currency[]
}

export interface Currency {
    sISOCode: string;
    sName: string;
}