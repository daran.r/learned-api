export interface UserSoapResponseEnvelope {
    body: UserSoapResponseBody
}

interface UserSoapResponseBody {
    response: UserSoapResponse
}

interface UserSoapResponse {
    result: UserSoapResult
}

interface UserSoapResult {
    tUser: User[]
}

export interface User {
    sAge: string
    sName: string
}