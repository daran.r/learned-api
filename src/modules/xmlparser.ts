import { Parser, Builder, processors } from 'xml2js';

const parser = new Parser({
    trim: true,
    explicitArray: false,
    explicitRoot: false,
    tagNameProcessors: [processors.stripPrefix, processors.firstCharLowerCase]
});

const builder = new Builder({
    xmldec: { version: '1.0', encoding: 'utf-8' },
    // renderOpts: { pretty: false }
});

const parseJSONToXml = (json: object): string => {
    return builder.buildObject(json);
}

const parseXmlToJson = async <T>(xml: string): Promise<T> => {
    return parser.parseStringPromise(xml);
}

export {
    parseJSONToXml,
    parseXmlToJson
}