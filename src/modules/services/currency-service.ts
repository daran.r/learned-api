import axios, { AxiosResponse } from 'axios';
import { CurrencySoapResponseEnvelope,Currency } from '../types/currency';
import { parseJSONToXml, parseXmlToJson } from '../xmlparser';

const url = "http://webservices.oorsprong.org/websamples.countryinfo/CountryInfoService.wso";

const getCurrenciesFromJsonAPI = async (): Promise<Currency []> => {
    console.log('hit json api');
    const result: AxiosResponse = await axios.get(`${url}/ListOfCurrenciesByName/JSON/debug?`);
    const currencies: Currency[] = result.data;
    return currencies;
};

const getCurrenciesFromSOAPService = async (): Promise<Currency []> => {
    console.log('hit soap api');
    const requestObj = {
        "soap:Envelope":
        {
            "$": {
                "xmlns:soap": "http://schemas.xmlsoap.org/soap/envelope/"
            },
            "soap:Body": [
                {
                    "ListOfCurrenciesByName": [
                        { "$": { "xmlns": "http://www.oorsprong.org/websamples.countryinfo" } }]
                }
            ]
        }
    }
    const payload = parseJSONToXml(requestObj);
    const options = {
        headers: {
            'content-type': 'text/xml; charset=utf-8'
        }
    }

    const result: AxiosResponse = await axios.post(url, payload, options);
    const response = await 
        parseXmlToJson<CurrencySoapResponseEnvelope>(result.data);
    const currencies = response
        .body
        .listOfCurrenciesByNameResponse
        .listOfCurrenciesByNameResult
        .tCurrency;

    return currencies;
};

export {
    getCurrenciesFromJsonAPI,
    getCurrenciesFromSOAPService
}