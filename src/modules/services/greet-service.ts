export default class HelloService {
    public greetUser(name: string): string {
        return name ? `Hello ${name}` : 'Hello';
    }
}