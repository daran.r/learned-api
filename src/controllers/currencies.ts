import { Request, Response, NextFunction } from 'express';
import {
    getCurrenciesFromJsonAPI,
    getCurrenciesFromSOAPService
} from '../modules/services/currency-service'

const get = async (req: Request, res: Response, next: NextFunction) => {    
    const type = req.query.type;
    try {
        if (!type || type === 'rest' || type === 'soap') {
            let currencies;
            if (type === 'soap') {
                currencies = await getCurrenciesFromSOAPService();
            }
            else {
                currencies = await getCurrenciesFromJsonAPI();
            }

            res.status(200).json({ data: { currencies } });
        }
        else {
            res.status(400).send({error: 'invalid type'})
        }
    }
    catch (error) {
        next(error);
    }
};

export default { get };