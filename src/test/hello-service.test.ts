import HelloService from "../modules/services/greet-service"

const instance = new HelloService();

describe('When the guest name is provided', () => {
    test('The guest should be greeted with name', () => {
        expect(instance.greetUser("Alan")).toBe("Hello Alan");
    })
})

describe('When the guest is anonymous', () => {
    test('The guest should be greeted with hello', () => {
        expect(instance.greetUser("")).toBe("Hello");
    })
})