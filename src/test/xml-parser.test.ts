import { parseJSONToXml, parseXmlToJson } from "../modules/xmlparser";
import { UserSoapResponseEnvelope } from "../modules/types/user";

describe('When the JSON object is provided', () => {
    test('The xml should be returned', () => {
        const expectedXml = `<?xml version="1.0" encoding="utf-8"?>\n<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">\n  <soap:Body>\n    <ListOfCurrenciesByName xmlns="http://www.oorsprong.org/websamples.countryinfo"/>\n  </soap:Body>\n</soap:Envelope>`;

        const obj = {
            "soap:Envelope":
            {
                "$": {
                    "xmlns:soap": "http://schemas.xmlsoap.org/soap/envelope/"
                },
                "soap:Body": [
                    {
                        "ListOfCurrenciesByName": [
                            { "$": { "xmlns": "http://www.oorsprong.org/websamples.countryinfo" } }]
                    }
                ]
            }
        }
        const xml = parseJSONToXml(obj);
        expect(xml).toBe(expectedXml);
    })
})

describe('When XML is provided', () => {
    test('The valid JSON object should be returned', async () => {
        const xml = '<?xml version="1.0" encoding="utf-8"?>' +
            '<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">' +
            '<soap:Body><ListOfCurrenciesByName xmlns="http://www.oorsprong.org/websamples.countryinfo"></ListOfCurrenciesByName></soap:Body>' +
            '</soap:Envelope>';

        const expectedJSON = {
            "$": {
                "xmlns:soap": "http://schemas.xmlsoap.org/soap/envelope/"
            },
            "body": {
                "listOfCurrenciesByName":
                    { "$": { "xmlns": "http://www.oorsprong.org/websamples.countryinfo" } }
            }
        }

        const json = await parseXmlToJson(xml);
        expect(json).toEqual(expectedJSON);
    })

    test('The valid JSON array should be returned', async () => {
        const xml = `<?xml version="1.0" encoding="utf-8"?>
        <soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
            <soap:Body>
                <m:Response xmlns:m="http://mydummyurl.com/users">
                    <m:Result>
                        <m:tUser>
                            <m:sAge>23</m:sAge>
                            <m:sName>Jhon</m:sName>
                        </m:tUser>
                        <m:tUser>
                            <m:sAge>34</m:sAge>
                            <m:sName>Alex</m:sName>
                        </m:tUser>
                    </m:Result>
                </m:Response>
            </soap:Body>
        </soap:Envelope>`;

        const expectedJSON = {
            "$": {
                "xmlns:soap": "http://schemas.xmlsoap.org/soap/envelope/"
            },
            "body": {
                "response": {
                    "$": {
                        "xmlns:m": "http://mydummyurl.com/users"
                    },
                    "result": {
                        "tUser": [{ "sAge": "23", "sName": "Jhon" }, { "sAge": "34", "sName": "Alex" }]
                    }
                }
            }
        }

        const json = await parseXmlToJson<UserSoapResponseEnvelope>(xml);
        expect(json).toEqual(expectedJSON);
    })

    test('The json should have casted to respective type interface', async () => {
        const xml = `<?xml version="1.0" encoding="utf-8"?>
        <soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
            <soap:Body>
                <m:Response xmlns:m="http://mydummyurl.com/users">
                    <m:Result>
                        <m:tUser>
                            <m:sAge>23</m:sAge>
                            <m:sName>Jhon</m:sName>
                        </m:tUser>
                        <m:tUser>
                            <m:sAge>34</m:sAge>
                            <m:sName>Alex</m:sName>
                        </m:tUser>
                    </m:Result>
                </m:Response>
            </soap:Body>
        </soap:Envelope>`;

        const json = await parseXmlToJson<UserSoapResponseEnvelope>(xml);
        expect(json.body.response.result.tUser[0].sName).toEqual("Jhon");
    })
})