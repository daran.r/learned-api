import { 
    getCurrenciesFromJsonAPI, 
    getCurrenciesFromSOAPService 
} from '../modules/services/currency-service';

describe('When the currency api is called', () => {
    test('Should return atleast 1 currency from JSON API', async () => {
        const currencies = await getCurrenciesFromJsonAPI();
        expect(currencies.length).toBeGreaterThan(0);
    })  
    
    test('Should return atleast 1 currency from SOAP Endpoint', async () => {
        const currencies = await getCurrenciesFromSOAPService();
        expect(currencies.length).toBeGreaterThan(0);
    })
})