> ### Project Node (Express + Typescript) REST API, serves assets from an open API

# Getting started
To get the API running locally:
- Clone this repo
- Command `npm install` to install all required dependecies
- Command `npm run dev` to start the server running locally

To launch API in deployment mode
- Command `npm run build` to compile the typescript to js
- Command `npm run start` to launch the server 

Testing
- Command `npm run test` to execute the jest suite

ESLint
- Command `npm run lint` to locate improper or inconsistent coding

## Dependencies
- [typescript](https://www.typescriptlang.org/) - Helps development as a strongly typed syntax over javascript
- [expressjs](https://github.com/expressjs/express) - The library acts as the server for handling and routing HTTP requests
- [axios](https://axios-http.com/docs/intro) - A promise based HTTP client for node
- [morgan](https://www.npmjs.com/package/morgan) - HTTP request logger middleware for node.js
- [xml2js](https://www.npmjs.com/package/xml2js) - Library to parse JSON to XML vice versa
- [jest](https://jestjs.io/) - JavaScript testing framework

## CI/CD
Gitlab pipeline is configured to execute the test suite on every commit

### Deployement
The node server is deployed on [render](https://render.com/)
The deployment should be manually triggerd on the Gitlab pipeline

### Preview
- [default](https://learned-api-demo.onrender.com/currencies)
- [rest](https://learned-api-demo.onrender.com/currencies?type=rest)
- [soap](https://learned-api-demo.onrender.com/currencies?type=soap)
- [error](https://learned-api-demo.onrender.com/currencies?type=something)

# Assignment
Back-end (Node.js | Express)
### Rules:
- The code should be located in a repository (For instance: GitLab);
- Implement the project using Node.js + Express, and TypeScript.
- Feel free to use any state library or any additional packages you need.
- Demonstrate/reveal your knowledge in project implementation.
- Write clean reusable code, organised in a transparent scalable structure.

### Requirements:
 1.  Create a server with an API that returns a list of assets, obtained from a third party
provider.
2. When making a request to this API you should be able to choose if the data should come
from their REST or their SOAP interfaces. The default option should be REST.
3. Create a service module that fetches the data (using both REST and SOAP) from the
provider’s API

### Final checklist:
- [x] It is possible to run the server locally.
- [x] Api request without type return assets list.
- [x] Api request with type = rest return assets list.
- [x] Api request with type = soap return assets list.